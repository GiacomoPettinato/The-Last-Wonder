Platform 2.5D, metroidvania-like. 
Help Samara to explore a mystical world, Skytlantide.


OVERVIEW

​First project as game programmer

3 weeks of development

15 members (5 designers, 3 programmers, 3 concept artists, 4 3D artists)


WHAT I DID

​Main managers (GameManager and LevelManager)

Level design events

Behaviours of env objects

Dialogue system and text importing handlers

UI